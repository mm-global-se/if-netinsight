mmcore.IntegrationFactory.initialize('NetInsight', {
    campaign: '<Campaign Name>',
    slot: {
      campaign: <Slot Number>,
      experience: <Slot Number>
    },
    redirect: false,
    callback: function () {}
});