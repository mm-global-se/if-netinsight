mmcore.IntegrationFactory.register('NetInsight', {
    defaults: {},

    validate: function (data) {
        if (!data.campaign)
            return 'No campaign.';

        if (!data.slot)
            return 'No slot provided.';        

        if (!data.slot.campaign)
            return 'No slot for campaign name provided.';

        if (!data.slot.experience)
            return 'No slot for experience provided.';

        return true;
    },

    exec: function (data) {
        var mode = data.isProduction ? 'MMProd_' : 'MMSand_',
            campaign = data.campaign.replace(/[\_\s]/g, ''),
            experience = data.campaignInfo
                        .replace(data.campaign + '=', '')
                        .replace(/[\_\s]/g, '');

        !window.MI_TAG_CS && (window.MI_TAG_CS = '');
        window.MI_TAG_CS += '&' + data.slot.campaign + '=' + mode + campaign +
                            '&' + data.slot.experience + '=' + experience;

        if (typeof data.callback === 'function') data.callback.call(null, data.campaignInfo);
        return true;
    }
});