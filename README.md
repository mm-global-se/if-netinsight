# NetInsight

---

[TOC]

## Overview
This integration is used for extracting campaign experience data from the Maxymiser platform and sending it to [NetInsight](http://netinsight.net/).

## How we send the data

Campaign generation info is being send to NetInsight via populating `window.MI_TAG_CS` variable with campaign name and experience.

__Note:__ underscores and spaces will be removed the final output string

Slot numbers are used to hold the campaign data. __They are unique for each campaign!__

_See example_:

```javascript

window.MI_TAG_CS += '150=MMProdT01Button&151=Element1:Variant1|Element2:Default'

```

## Data Format

The data sent to Kissmetrics will be in the following format:

`<mode>_<campaign name>=<element1>:<variant>|<element2>:<variant>`

Please find additional information on the format of the sent data [here](https://bitbucket.org/mm-global-se/integration-factory#markdown-header-introduction
)

## Download

* [netinsight-register.js](https://bitbucket.org/mm-global-se/if-netinsight/src/master/src/netinsight-register.js)

* [netinsight-initialize.js](https://bitbucket.org/mm-global-se/if-netinsight/src/master/src/netinsight-initialize.js)

## Prerequisite

+ Campaign name

+ Slot number for campaign name

+ Slot number for campaign experience

__Note: Slots are unique for each campaign!__

## Deployment instructions

### Content Campaign

+ Ensure that you have Integration Factory plugin deployed on site level and has _order: -10_ ([find out more](https://bitbucket.org/mm-global-se/integration-factory))

+ Create another site script with _order: -5_ and add the code from [netinsight-register.js](https://bitbucket.org/mm-global-se/if-netinsight/src/master/src/netinsight-register.js) into it

+ Create campaign script with _order: > -5_, then customize the code from [netinsight-initialize.js](https://bitbucket.org/mm-global-se/if-netinsight/src/master/src/netinsight-initialize.js) and add into it

### Redirect Campaign

For redirect campaigns, the campaign script needs to be mapped to both the generation and redirected page. To achieve this follow the steps bellow:

+ Go through the instructions outlined in [Content Campaign](https://bitbucket.org/mm-global-se/if-ga#markdown-header-content-campaign)

+ Make sure that in the initialize campaign script, redirect is set to true.

+ Create and map an additional page where the redirect variant would land on. 

+ Map the integration initialize campaign script to this page as well.

## Possible Issues

### Timing issue

Maxymiser campaign can be generated at any time before or after DOM Ready event

# QA

Check `window.MI_TAG_CS` variable in DevTools console - it should contain desired campaign experience(s)